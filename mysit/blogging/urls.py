from django.urls import path
from blogging.views import PostListView, PostDetailView
from django.urls import path, include
from rest_framework import routers

urlpatterns = [
    path('', PostListView.as_view(), name="blog_index"),
    path('posts/<int:pk>/', PostDetailView.as_view(), name="blog_detail"),
]